---
title: "Getting a stack trace with debug symbols out of a flatpak"
summary: "This Tech Note explores how to setup debug symbols fot get stack trace for a Flatpak application."
weight: 1
draft: false
slug: tn001
date: 2020-06-03
author: "Hubert Figuière"
tags: ["flatpak"]
license: https://creativecommons.org/licenses/by/4.0/
---

_This article was originally published at
[https://www.figuiere.net/hub/blog/?2019/10/11/878-getting-a-stack-trace-out-of-a-flatpak](https://www.figuiere.net/hub/blog/?2019/10/11/878-getting-a-stack-trace-out-of-a-flatpak)
in October 2019 and this is an updated version._

The Flatpak application you use just crashed.

How do you report it? If you file a bug just saying it crashed, the
developers will probably wish and ask for a stack trace. On Fedora 30,
for example, `abrt`, the crash reporting system, doesn't provide any
useful information. Let's see if we can extract that information.

We are gonna have to use the terminal to use some command line
tools. Flatpak has a tool `flatpak-coredumpctl` to use the core dump
in the flatpak sandbox. The core dump is an image of the program
memory when it crashed, image that will contain a lot of information
about the crash. By default the tool will not be able to provide much
useful information. There is some initial setup needed to be able to
have a better output.

First you must make sure that you have the right _debug_ package for
the right version of the Flatpak runtime. Well, actually, for the
corresponding SDK.

Check the runtime and version associated to the application:

```shell
$ flatpak info org.gnome.MyApp
```

In the output of that command, check the line that starts with Sdk. It
could be:

```shell
Sdk: org.gnome.Sdk/x86_64/3.34
```

What is after `Sdk:` could be a different value, and that is the
information we are looking for. `org.gnome.Sdk` is the SDK identifier,
`x86_64` is the architecture, and `3.34` is the version.

First, you need to install the SDK: `flatpak install --user
org.gnome.Sdk/x86_64/3.34`. The _debug_ package has the same name with
a suffix of `.Debug`. So in that case you can use the command `flatpak
install --user org.gnome.Sdk.Debug/x86_64/3.34` to install it. The is
the one in our example, you probably to adjust to your own
case. *WARNING:* it is likely a big download. For example the debug
package for GNOME 3.34 on x86_64 is 3.4GB. I recommend installing it
for the user, as, unless you have a lot of space on your system
filesystem (if separate), it will fail.

Then you need to install the debug info for the app. It is the
application identifier suffixed with `.Debug`. In that case
`org.gnome.MyApp.Debug`.

Both will provide the debugging information that is necessary to be
able to see where things crashed in the code.


Now you can run:

```shell
$ flatpak-coredumpctl org.gnome.MyApp
```

This is launching `gdb` inside the flatpak sandbox. It will take a
while to process, and use quite a good chunk of memory. What follows
is mostly for people that are not familiar with `gdb` ; if you are,
just go ahead, you know what to do.

When it is ready, something like this will be printed in the terminal:

```shell
Program terminated with signal SIGSEGV, Segmentation fault.
```

And then at the end, the prompt: `(gdb)`

You can type `where` and press "Enter".  This prints the stack trace:
lines starting with #NNN where NNN is an increasing number.  You can
copy that output and provide it to the developers in the bug report.

Then you can type `quit` followed by "Enter" when you are done.

I hope that has been useful to you.
