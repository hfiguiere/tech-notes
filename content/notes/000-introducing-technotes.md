---
title: "Introducing Tech Notes"
summary: "What the Tech Notes are about."
weight: 1
draft: false
slug: tn000
date: 2020-04-19
author: "Hubert Figuière"
tags: ["general"]
license: https://creativecommons.org/licenses/by/4.0/
---

Have you ever ran into a problem and trying to figure out how things
work, but documentation is spread out everywhere? This has happened to
me often, and recently, and while search engines for web content are
tools we couldn't leave without, it is often overwhelming. I started
writing documents in my text editor on how to do these
things. Sometime the result ended up in a blog post, sometime not, and
it is to chaotic in my books to provide technical reference. This is
how the idea of the *Tech Notes* came.

*Tech Notes* are short format tech documents on topics I wanted to
understand and explain, covering subjects around Linux, software
development and libre applications.

## Format

The format is meant to be short, covering one subject well, but not
too extensively. Ideally cross referencing will be done to indicate
related topics covered elsewhere.

The notes are meant to be published on the web, and at the time of
writing this is done using [Hugo](https://gohugo.io/), a static
content management system.

The Tech notes will be numbered in an order close to publication
order. This one is TN:000 and I hope more will come.

There is no defined frequency of publication.

## Updates

Whenever possible, mistakes and updates will be addressed in the
corresponding Tech Note, and a list of changes will be published. If
content is completely obsolete or need a overhaul, a new Tech Note
will be published whenever possible.

## License

Unless mentionned otherwise, the Tech Notes are under the [Creative
Commons Attribution 4.0 International (CC BY 4.0)
license](https://creativecommons.org/licenses/by/4.0/). Information
should be libre.
